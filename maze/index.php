<?php
include "ResponseCodeFix.php";
include "Maze.php";
include "DataBase.php";

function CreateEndpoint($endpoint,$path,$id)
{
    $content = '<?php $id='.$id.";".file_get_contents("templates/".$endpoint."/index.php");
    if (!file_exists($path."/".$endpoint)) {
        mkdir($path."/".$endpoint, 0777, true);
    }
    $file = fopen($path."/".$endpoint."/index.php","wb");
    fwrite($file,$content);
    fclose($file);
}

    if($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $input = json_decode(file_get_contents("php://input"), true);
        
        $maze = new Maze($input);
        $db = new DataBase();
        
        if($maze ->Validate())
        {
            //////////////////////////////////////////////////////////////////////////////////////
            // http://www.kariera.allegro.pl/oferta/stazysta-w-zespole-software-development/150198
            //////////////////////////////////////////////////////////////////////////////////////
            // "...jeżeli labirynt jest poprawny odpowie odpowiednim statusem http potwierdzającym 
            // dodanie i zwróci id tego labiryntu..."
            //////////////////////////////////////////////////////////////////////////////////////
            // Funkcjonalność interpretuję jako kod 201 "CREATED"
            //////////////////////////////////////////////////////////////////////////////////////
            
            $dataToSend = $maze->Prepare();
            $columns = array();
            $values = array();
            foreach($dataToSend as $key => $value)
            {
                array_push($columns, $key);
                array_push($values, $value);
            }
            
            echo $id = $db->Insert("mazes", $columns, $values);
            
            $path = $_SERVER['DOCUMENT_ROOT'].'/aMAZEme/maze/'.$id;
                
            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }
            
            CreateEndpoint("describe", $path, $id);
            CreateEndpoint("exit", $path, $id);
            CreateEndpoint("quotation", $path, $id);
            CreateEndpoint("path", $path, $id);

            http_response_code(201);

        }else{
            //////////////////////////////////////////////////////////////////////////////////////
            // Funkcjonalność interpretuję jako kod 400 "BAD REQUEST"
            //////////////////////////////////////////////////////////////////////////////////////
            http_response_code(400);
        }
        
    }else{
        echo "<h1>aMAZE|me</h1><br/>";
        echo "<h3>Mateusz Bartos</h3><br/>";
        echo "<hr/>";
        echo "ERROR: NO PUT DATA RECEIVED<br/>";
        echo "<img src='https://bitbucket-assetroot.s3.amazonaws.com/repository/jrE9M8/2944984267-amazeme.png?Signature=esaaKfavzTDqZ1JCt3aM1TV%2FzUU%3D&Expires=1430689012&AWSAccessKeyId=0EMWEFSGA12Z1HF1TZ82'>";
    }
?>