<?php
class Test {
    
    var $data;
    var $data_string;
    
    function __construct() {
        $this->data = array("maze" => array(
                                      array(1,1,1,1,1,1,1),
                                      array(1,0,0,0,0,0,1),
                                      array(1,0,1,0,1,0,1),
                                      array(1,0,1,0,1,0,0),
                                      array(1,0,1,0,1,1,1),
                                      array(1,0,1,0,1,0,1),
                                      array(1,0,0,0,0,0,1),
                                      array(1,0,1,1,1,1,1)
            ), "entrance" => array(7,1));
        $this->data_string = json_encode($this->data);
    }
    
    function Send()
    {
        $ch = curl_init('http://mateuszbartos.com/aMAZEme/maze/');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");   
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($this->data_string))
        );
        
        $result = curl_exec($ch);
//        $info = curl_getinfo($ch);
        curl_close($ch);
//        var_dump($info);
        echo $result;
    }
}

$test = new Test();
$test->Send();
