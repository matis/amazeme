 <?php
class Maze {
    private $maze;
    
    private $width;
    private $height;
    
    private $entrance;
    private $walls;
    private $describe;
    
    private $tiles;
    
    private $exit;
    
    private $length = 0;
    
    public $validExits = true;
    
    
    function __construct($inputData) {
        $this-> maze = $inputData["maze"];
        $this-> entrance = $inputData["entrance"];
        
        $this-> SetWidth();
        $this-> SetHeight();
    }
    
    function SetWidth()
    {
        $this-> width = count($this->maze[0]);
    }
    
    function SetHeight()
    {
        $this-> height = count($this->maze);
    }
    
    function Validate(){
        if($this->ValidateSize() &&
           $this->Analyze())
        {

            return true;
        }else{
            return false;
        }
    }
    
    function ValidateSize()
    {
        if($this-> width < 1 && $this-> height < 1) 
        {
            return false;
        }
        
        foreach($this->maze as $row)
        {
            if(count($row) !== $this-> width)
            {
                return false;
            }
        }
        return true;
    }
    
    function Analyze()
    {
        $exit = array(null,null);
        $walls = 0;
        $tiles = 0;
        for($i = 0 ; $i < $this->height ; $i++)
        {
            for($j = 0 ; $j < $this->width ; $j++)
            {
                //count walls and tiles
                ($this->maze[$i][$j]) ? $walls++ : $tiles++;
                
                if(!$this->maze[$i][$j] && (($i === 0 || $i === $this->height -1) || ($j === 0 || $j === $this->width-1)))
                {
                    //Check if exit
                    if(($i !== $this-> entrance[0]) || ($j !== $this-> entrance[1]))
                    {
                        //hole detected - no entrance
                        if(isset($exit[0]) && isset($exit[1]))
                        {
                            $this-> validExits = false;
                        }else{
                            $exit[0] = $i;
                            $exit[1] = $j;
                        }
                    }
                }
            }
        }
        
        $this->exit = $exit;
        $this->walls = $walls;
        $this->tiles = $tiles;
        $this->describe = json_encode(array("walls" => $walls, "corridors" => $tiles));
        if(!isset($exit[0]) && !isset($exit[1]))
        {
            $this-> validExits = false;
        }
        $this->DFS($this->entrance[0],$this->entrance[1]);
        
        if($this-> validExits){
            return true;
        }else{
            return false;
        }
    }
    
    private $visited = array(array());
    private $trail = array();
    private $trailString = "";
    
    function DFS($y, $x)
    {
        $this->visited[$y][$x] = true;
        array_push($this->trail, "[".$y.",".$x."]");
        if($this->exit[0] === $y && $this->exit[1] === $x){
            $this->length = count($this->trail);
            $this->trailString = "[ ";
            foreach($this->trail as $value){
                $this->trailString .= $value.",";
            }
            $this->trailString = substr($this->trailString, 0, -1);
            $this->trailString .= " ]";
            return true;
        }
        
        //move right
        if(!$this->maze[$y][$x+1] && ($x < $this->width - 1) && (!isset($this->visited[$y][$x+1]))){
            $this->DFS($y,$x+1);
        }
        //move left
        if(!$this->maze[$y][$x-1] && ($x > 0) && (!isset($this->visited[$y][$x-1]))){
            $this->DFS($y,$x-1);
        }
        //move down
        if(!$this->maze[$y+1][$x] && ($y < $this->height -1) && (!isset($this->visited[$y+1][$x]))){
            $this->DFS($y+1,$x);
        }
        //move up
        if(!$this->maze[$y-1][$x] && ($y > 0) && (!isset($this->visited[$y-1][$x]))){
            $this->DFS($y-1,$x);
        }
        array_pop($this->trail);
        return false;
        
    }
    
    function Prepare()
    {
        return array(
            "exits" => "[".$this->exit[0].",".$this->exit[1]."]",
            "walls" => $this->walls,
            "corridors" => $this->tiles,
            "length" => $this->length,
            "path" => $this->trailString
        );
    }
    
}