<?php
class DataBase {
    private $server, $login, $password, $db;
    public $m;
    
    function __construct() {
        $this->server = 'SERVER';
        $this->login = 'LOGIN';
        $this->password = 'PASSWORD';
        $this->db = 'DATABASE';
        
        $this->m = new mysqli($this->server, $this->login, $this->password, $this->db);
        if ($this->m ->connect_error) {
            die("Connection failed");
        }
        $this->m ->set_charset("utf8");
        

    }
    
    public function Insert($table, $columns, $values)
    {
        if(count($columns) != count($values))
        {
            echo "Number of columns and values is not equal";
            return false;
        }
        
        $query = "INSERT INTO ".$table." (";
        
        foreach($columns as $column)
        {
            $query .= $column.",";
        }
        $query = substr($query,0,-1);
        $query .= ") VALUES (";
        
        foreach($values as $value)
        {
            $query .= "'".$value."',";
        }
        $query = substr($query,0,-1);
        $query .= ")";
        
        $this->m ->query($query);
        
        return $this->m ->insert_id;
//        return $this->m ->error;
    }
    
    public function Update($table, $columns, $values, $target)
    {
        if(count($columns) != count($values))
        {
            echo "Number of columns and values is not equal";
            return false;
        }
        
        $query = "UPDATE ".$table." SET (";
        
        $i = 0;
        foreach($columns as $column)
        {
            $query .= $column." = '".$values[$i]."', ";
            $i++;
        }
        
        $query = substr($query,0,-2);
        $query .= ") WHERE ".$target;
        
        return $this->m ->query($query);
//        return $this->m ->error;
    }
    
    public function Select($table, $columns, $target)
    {   
        $query = "SELECT ";
        
        foreach($columns as $column)
        {
            $query .= $column.", ";
        }
        
        $query = substr($query,0,-2);
        $query .= " FROM ".$table;
        
        if(isset($target))
        {
            $query .= " WHERE ".$target;
        }
        
        $result = $this->m ->query($query);
        if($result)
        {
            return $result;
        }else{
            return false;
//            return $this->m ->error;
        }
    }
    
}
